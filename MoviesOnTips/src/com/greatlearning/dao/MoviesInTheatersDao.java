package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


import com.greatlearning.bean.MoviesInTheaters;
import com.greatlearning.resource.DbConnection;

public class MoviesInTheatersDao {
	
	public List<MoviesInTheaters> findAllMoviesInTheaters() {
		List<MoviesInTheaters> listOfMoviesInTheaters = new ArrayList<>();
		try {

			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from MoviesInTheaters ");
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				MoviesInTheaters moviesInTheaters = new MoviesInTheaters();

				moviesInTheaters.setId(rs.getInt(1));
				moviesInTheaters.setTitle(rs.getString(2));
				moviesInTheaters.setYear(rs.getInt(3));
				moviesInTheaters.setContentRating(rs.getString(6));
				moviesInTheaters.setDuration(rs.getString(7));
				moviesInTheaters.setReleaseDate(rs.getDate(8));
				moviesInTheaters.setImdbRating(rs.getInt(9));

				listOfMoviesInTheaters.add(moviesInTheaters);
			}
		} catch (Exception e) {
			System.out.println("In findAllMoviesInTheaters method " + e);
		}
		return listOfMoviesInTheaters;
	}


}
