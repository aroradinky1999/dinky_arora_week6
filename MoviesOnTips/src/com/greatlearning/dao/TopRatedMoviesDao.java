package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.TopRatedMovies;
import com.greatlearning.resource.DbConnection;

public class TopRatedMoviesDao {
	
	public List<TopRatedMovies> findAllTopRatedMovies() {
		List<TopRatedMovies> listOfTopRatedMovies = new ArrayList<>();
		try {

			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from TopRatedMovies");
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				TopRatedMovies topRatedMovies = new TopRatedMovies();

				topRatedMovies.setId(rs.getInt(1));
				topRatedMovies.setTitle(rs.getString(2));
				topRatedMovies.setYear(rs.getInt(3));
				topRatedMovies.setContentRating(rs.getString(6));
				topRatedMovies.setDuration(rs.getString(7));
				topRatedMovies.setReleaseDate(rs.getDate(8));
				topRatedMovies.setImdbRating(rs.getInt(9));

				listOfTopRatedMovies.add(topRatedMovies);
			}
		} catch (Exception e) {
			System.out.println("In findAllTopRatedMovies method " + e);
		}
		return listOfTopRatedMovies;
	}


}
