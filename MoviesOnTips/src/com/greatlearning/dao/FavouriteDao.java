package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.Favourite;
import com.greatlearning.resource.DbConnection;

public class FavouriteDao {
	
	public List<Favourite> findAllFavouriteMovies() {
		List<Favourite> listOfFavouriteMovies = new ArrayList<>();
		try {

			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from Favourite");
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Favourite favourite = new Favourite();

				favourite.setId(rs.getInt(1));
				favourite.setTitle(rs.getString(2));
				favourite.setYear(rs.getInt(3));
				favourite.setContentRating(rs.getString(6));
				favourite.setDuration(rs.getString(7));
				favourite.setReleaseDate(rs.getDate(8));
				favourite.setImdbRating(rs.getInt(9));

				listOfFavouriteMovies.add(favourite);
			}
		} catch (Exception e) {
			System.out.println("In findAllFavouriteMovies method " + e);
		}
		return listOfFavouriteMovies;
	}


}
