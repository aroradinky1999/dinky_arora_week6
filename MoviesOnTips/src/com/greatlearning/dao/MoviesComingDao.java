package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.MoviesComing;
import com.greatlearning.resource.DbConnection;

public class MoviesComingDao {

	public List<MoviesComing> findAllMoviesComing() {
		List<MoviesComing> listOfMoviesComing = new ArrayList<>();
		try {

			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from MoviesComing");
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				MoviesComing moviesComing = new MoviesComing();

				moviesComing.setId(rs.getInt(1));
				moviesComing.setTitle(rs.getString(2));
				moviesComing.setYear(rs.getInt(3));
				moviesComing.setContentRating(rs.getString(6));
				moviesComing.setDuration(rs.getString(7));
				moviesComing.setReleaseDate(rs.getDate(8));
				moviesComing.setImdbRating(rs.getInt(9));

				listOfMoviesComing.add(moviesComing);
			}
		} catch (Exception e) {
			System.out.println("In findAllMoviesComing method " + e);
		}
		return listOfMoviesComing;
	}

}
