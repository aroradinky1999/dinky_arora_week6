package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.TopRatedIndian;
import com.greatlearning.resource.DbConnection;

public class TopRatedIndianDao {
	
	public List<TopRatedIndian> findAllTopRatedIndianMovies() {
		List<TopRatedIndian> listOfTopRatedIndianMovies = new ArrayList<>();
		try {

			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from TopRatedIndian");
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				TopRatedIndian topRatedIndian = new TopRatedIndian();

				topRatedIndian.setId(rs.getInt(1));
				topRatedIndian.setTitle(rs.getString(2));
				topRatedIndian.setYear(rs.getInt(3));
				topRatedIndian.setContentRating(rs.getString(6));
				topRatedIndian.setDuration(rs.getString(7));
				topRatedIndian.setReleaseDate(rs.getDate(8));
				topRatedIndian.setImdbRating(rs.getInt(9));

				listOfTopRatedIndianMovies.add(topRatedIndian);
			}
		} catch (Exception e) {
			System.out.println("In findAllTopRatedIndianMovies method " + e);
		}
		return listOfTopRatedIndianMovies;
	}

}
