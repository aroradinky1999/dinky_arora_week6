package com.greatlearning.factory;

import com.greatlearning.bean.Favourite;
import com.greatlearning.bean.MoviesComing;
import com.greatlearning.bean.MoviesInTheaters;
import com.greatlearning.bean.TopRatedIndian;
import com.greatlearning.bean.TopRatedMovies;
import com.greatlearning.interfac.Movies;

public class MoviesFactory {

	public static Movies getInstance(String type) {
		if(type.equalsIgnoreCase("MoviesComing")) {
			return new MoviesComing();
		}else if(type.equalsIgnoreCase("MoviesInTheaters")) {
			return new MoviesInTheaters();
		}else if(type.equalsIgnoreCase("TopRatedIndian")) {
			return new TopRatedIndian();
		}else if(type.equalsIgnoreCase("TopRatedMovies")) {
			return new TopRatedMovies();
		}else if(type.equalsIgnoreCase("Favourite")) {
			return new Favourite();
		}else {
			return null;
		}
	}
}
