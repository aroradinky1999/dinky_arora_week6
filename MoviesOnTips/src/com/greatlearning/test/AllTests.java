package com.greatlearning.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DbConnectionTest.class, FavouriteDaoTest.class, FavouriteTest.class, MoviesComingDaoTest.class,
		MoviesComingTest.class, MoviesFactoryTest.class, MoviesInTheatersDaoTest.class, MoviesInTheatersTest.class,
		TopRatedIndianDaoTest.class, TopRatedIndianTest.class, TopRatedMoviesDaoTest.class, TopRatedMoviesTest.class })
public class AllTests {

}
