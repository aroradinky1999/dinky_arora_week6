package com.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.bean.MoviesComing;

public class MoviesComingTest {
	

	@Test
	public void testTypeOfMovies() {
		MoviesComing moviesComing=new MoviesComing();
		String msg = moviesComing.typeOfMovies();
		assertTrue(msg, true);
	}
	
	@Test
	public void testSetId() {
		MoviesComing moviesComing=new MoviesComing();
		moviesComing.setId(2);
		
	}

	@Test
	public void testGetId() {
		MoviesComing moviesComing=new MoviesComing();
		moviesComing.setId(2);
		assertTrue(moviesComing.getId()==2);
	}


	@Test
	public void testGetTitle() {
		MoviesComing moviesComing=new MoviesComing();
		moviesComing.setTitle("Hannah");
		assertTrue(moviesComing.getTitle()=="Hannah");

	}

	@Test
	public void testSetTitle() {
		MoviesComing moviesComing=new MoviesComing();
		moviesComing.setTitle("Hannah");
		
	}

}
