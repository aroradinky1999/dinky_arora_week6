package com.greatlearning.test;

import static org.junit.Assert.*;

import java.sql.Connection;

import org.junit.Test;

import com.greatlearning.resource.DbConnection;

public class DbConnectionTest {

	@Test
	public void testGetDbConnection() {
		 DbConnection  dbConnection = new  DbConnection();
		 Connection con = dbConnection.getDbConnection();
		 assertEquals(con!=null,true);
		 
	}	

}
