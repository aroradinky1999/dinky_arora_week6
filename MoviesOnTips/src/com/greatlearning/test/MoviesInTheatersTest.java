package com.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.bean.MoviesInTheaters;

public class MoviesInTheatersTest {

	@Test
	public void testTypeOfMovies() {
		MoviesInTheaters moviesInTheaters=new MoviesInTheaters();
		String msg = moviesInTheaters.typeOfMovies();
		assertTrue(msg, true);
	}

	@Test
	public void testGetId() {
		MoviesInTheaters moviesInTheaters=new MoviesInTheaters();
		moviesInTheaters.setId(2);
		assertTrue(moviesInTheaters.getId()==2);
	}

	@Test
	public void testSetId() {
		MoviesInTheaters moviesInTheaters=new MoviesInTheaters();
		moviesInTheaters.setId(2);
	}

	@Test
	public void testGetTitle() {
		MoviesInTheaters moviesInTheaters=new MoviesInTheaters();
		moviesInTheaters.setTitle("Agneepath");
		assertTrue(moviesInTheaters.getTitle()=="Agneepath");
	}

	@Test
	public void testSetTitle() {
		MoviesInTheaters moviesInTheaters=new MoviesInTheaters();
		moviesInTheaters.setTitle("Agneepath");
		
	}


}
