package com.greatlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.greatlearning.bean.MoviesInTheaters;

import com.greatlearning.dao.MoviesInTheatersDao;

public class MoviesInTheatersDaoTest {

	@Test
	public void testFindAllMoviesInTheaters() {
		MoviesInTheatersDao moviesInTheatersDao = new MoviesInTheatersDao();
		List<MoviesInTheaters> listOfMoviesInTheaters = moviesInTheatersDao.findAllMoviesInTheaters();
		assertEquals(5, listOfMoviesInTheaters.size());		
	}

}
