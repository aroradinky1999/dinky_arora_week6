package com.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.factory.MoviesFactory;
import com.greatlearning.interfac.Movies;

public class MoviesFactoryTest {

	@Test
	public void testGetInstanceMoviesComing() {
		Movies m = MoviesFactory.getInstance("MoviesComing");
		String msg = m.typeOfMovies();
		assertTrue(msg, true);
	}

	@Test
	public void testGetInstanceMoviesInTheaters() {
		Movies m = MoviesFactory.getInstance("MoviesInTheaters");
		String msg = m.typeOfMovies();
		assertTrue(msg, true);

	}
	
	@Test
	public void testGetInstanceTopRatedIndian() {
		Movies m = MoviesFactory.getInstance("TopRatedIndian");
		String msg = m.typeOfMovies();
		assertTrue(msg, true);

	}
	
	@Test
	public void testGetInstanceTopRatedMovies() {
		Movies m = MoviesFactory.getInstance("TopRatedMovies");
		String msg = m.typeOfMovies();
		assertTrue(msg, true);

	}
	
	@Test
	public void testGetInstanceFavourite() {
		Movies m = MoviesFactory.getInstance("Favourite");
		String msg = m.typeOfMovies();
		assertTrue(msg, true);

	}

}
