package com.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.bean.TopRatedIndian;

public class TopRatedIndianTest {

	@Test
	public void testTypeOfMovies() {
		TopRatedIndian topRatedIndian=new TopRatedIndian();
		String msg = topRatedIndian.typeOfMovies();
		assertTrue(msg, true);
	}

	@Test
	public void testGetId() {
		TopRatedIndian topRatedIndian=new TopRatedIndian();
		topRatedIndian.setId(2);
		assertTrue(topRatedIndian.getId()==2);
	}

	@Test
	public void testSetId() {
		TopRatedIndian topRatedIndian=new TopRatedIndian();
		topRatedIndian.setId(2);
	}

	@Test
	public void testGetTitle() {
		TopRatedIndian topRatedIndian=new TopRatedIndian();
		topRatedIndian.setTitle("Madaari");
		assertTrue(topRatedIndian.getTitle()=="Madaari");
	}

	@Test
	public void testSetTitle() {
		TopRatedIndian topRatedIndian=new TopRatedIndian();
		topRatedIndian.setTitle("Madaari");
	}

	

}
