package com.greatlearning.test;

import static org.junit.Assert.*;

import java.util.List;


import org.junit.Test;

import com.greatlearning.bean.TopRatedIndian;
import com.greatlearning.dao.TopRatedIndianDao;

public class TopRatedIndianDaoTest {


	@Test
	public void testFindAllTopRatedIndianMovies() {
		TopRatedIndianDao topRatedIndianDao = new TopRatedIndianDao();
		List<TopRatedIndian> listOfTopRatedIndian = topRatedIndianDao.findAllTopRatedIndianMovies();
		assertEquals(5, listOfTopRatedIndian.size());	
	}

}
