package com.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.bean.Favourite;

public class FavouriteTest {

	@Test
	public void testTypeOfMovies() {
		Favourite favourite=new Favourite();
		String msg = favourite.typeOfMovies();
		assertTrue(msg, true);
	}

	@Test
	public void testGetId() {
		Favourite favourite=new Favourite();
		favourite.setId(2);
		assertTrue(favourite.getId()==2);
	}

	@Test
	public void testSetId() {
		Favourite favourite=new Favourite();
		favourite.setId(2);
	}

	@Test
	public void testGetTitle() {
		Favourite favourite=new Favourite();
		favourite.setTitle("Paa");
		assertTrue(favourite.getTitle()=="Paa");

	}

	@Test
	public void testSetTitle() {
		Favourite favourite=new Favourite();
		favourite.setTitle("Paa");
	}


}
