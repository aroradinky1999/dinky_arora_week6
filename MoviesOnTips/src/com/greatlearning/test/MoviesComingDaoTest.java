package com.greatlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.greatlearning.bean.MoviesComing;
import com.greatlearning.dao.MoviesComingDao;

public class MoviesComingDaoTest {

	@Test
	public void testFindAllMoviesComing() {
		MoviesComingDao moviesComingDao = new MoviesComingDao();
		List<MoviesComing> listOfMoviesComing =  moviesComingDao.findAllMoviesComing();
		assertEquals(5, listOfMoviesComing.size());		
	}
	
}
