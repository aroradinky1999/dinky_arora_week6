package com.greatlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.greatlearning.bean.TopRatedMovies;
import com.greatlearning.dao.TopRatedMoviesDao;

public class TopRatedMoviesDaoTest {

	@Test
	public void testFindAllTopRatedMovies() {
		TopRatedMoviesDao topRatedMoviesDao = new TopRatedMoviesDao();
		List<TopRatedMovies> listOfTopRatedMoviesDao = topRatedMoviesDao.findAllTopRatedMovies();
		assertEquals(5, listOfTopRatedMoviesDao.size());	
	}

}
