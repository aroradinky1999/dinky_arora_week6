package com.greatlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.greatlearning.bean.Favourite;
import com.greatlearning.dao.FavouriteDao;


public class FavouriteDaoTest {

	@Test
	public void testFindAllFavouriteMovies() {
		FavouriteDao favouriteDao = new FavouriteDao();
		List<Favourite> listOfMoviesComing =  favouriteDao.findAllFavouriteMovies();
		assertEquals(5, listOfMoviesComing.size());		
	}

}
