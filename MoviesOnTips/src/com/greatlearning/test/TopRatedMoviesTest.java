package com.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.bean.TopRatedIndian;
import com.greatlearning.bean.TopRatedMovies;

public class TopRatedMoviesTest {

	@Test
	public void testTypeOfMovies() {
		TopRatedMovies topRatedMovies=new TopRatedMovies();
		String msg = topRatedMovies.typeOfMovies();
		assertTrue(msg, true);
	}

	@Test
	public void testGetId() {
		TopRatedMovies topRatedMovies=new TopRatedMovies();
		topRatedMovies.setId(2);
		assertTrue(topRatedMovies.getId()==2);
	}

	@Test
	public void testSetId() {
		TopRatedMovies topRatedMovies=new TopRatedMovies();
		topRatedMovies.setId(2);
	}

	@Test
	public void testGetTitle() {
		TopRatedMovies topRatedMovies=new TopRatedMovies();
		topRatedMovies.setTitle("Grottmannen Dug");
		assertTrue(topRatedMovies.getTitle()=="Grottmannen Dug");
	}

	@Test
	public void testSetTitle() {
		TopRatedMovies topRatedMovies=new TopRatedMovies();
		topRatedMovies.setTitle("Grottmannen Dug");
	}


}
