package com.greatlearning.bean;

import java.sql.Array;
import java.util.Date;

import com.greatlearning.interfac.Movies;

public class MoviesComing implements Movies{
	
	private int id;
	private String title;
	private int year;
	private Array genres;
	private Array ratings;
	private String contentRating;
	private String duration;
	private Date releaseDate;
	private int imdbRating;
	
	@Override
	public String typeOfMovies() {
		return "Movies coming soon will be shown here";
	}

	public MoviesComing() {
		super();		
	}

	public MoviesComing(int id, String title, int year, Array genres, Array ratings, String contentRating,
			String duration, Date releaseDate, int imdbRating) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.genres = genres;
		this.ratings = ratings;
		this.contentRating = contentRating;
		this.duration = duration;
		this.releaseDate = releaseDate;
		this.imdbRating = imdbRating;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Array getGenres() {
		return genres;
	}

	public void setGenres(Array array) {
		this.genres = array;
	}

	public Array getRatings() {
		return ratings;
	}

	public void setRatings(Array array) {
		this.ratings = array;
	}

	public String getContentRating() {
		return contentRating;
	}

	public void setContentRating(String contentRating) {
		this.contentRating = contentRating;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public int getImdbRating() {
		return imdbRating;
	}

	public void setImdbRating(int imdbRating) {
		this.imdbRating = imdbRating;
	}	

}
