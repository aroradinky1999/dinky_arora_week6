create database assignment6_dinky;
use assignment6_dinky;
create table MoviesComing(id int primary key,title varchar(20),year int ,genres JSON ,ratings JSON,contentRating varchar(10),duration varchar(10),releaseDate varchar(10),imdbRating int);
insert into MoviesComing values(1,'Game Night',2018,'["Action","Comedy","Crime"]','[2,10,1,6,5,7]','11','PT100M','2018-02-28',3);
insert into MoviesComing values(2,'Hannah',2017,'["Horror","Drama","Romance"]','[2,10,1,6,5,7]','R','PT95M','2017-02-26',5);
insert into MoviesComing values(3,'The Lodgers',2017,'["Drama"]','[2,10,1,6,5,7]','R','PT92M','2017-02-03',4);
insert into MoviesComing values(4,'The Chamber',2016,'["Horror","Thriller"]','[9,2,1,6,5,8]','11','PT88M','2016-02-09',3);
insert into MoviesComing values(5,'Red Sparrow',2017,'["Mystery","Thriller"]','[2,5,1,6,8,7]','R','PT139M','2017-02-06',2);
   
   select * from  MoviesComing;
   
create table MoviesInTheaters(id int primary key,title varchar(20),year int ,genres JSON ,ratings JSON,contentRating varchar(10),duration varchar(10),releaseDate varchar(10),imdbRating int);
insert into MoviesInTheaters values(1,'Neerja',2016,'["Action","Thriller","Crime"]','[2,10,1,6,5,7]','R','PT190M','2016-02-28',5);
insert into MoviesInTheaters values(2,'Agneepath',2019,'["Action","Thriller"]','[4,7,1,6,5,9]','11','PT78M','2019-02-08',4);
insert into MoviesInTheaters values(3,'Pokiri',2016,'["Action","Thriller","Crime"]','[9,10,5,6,9,7]','R','PT63M','2016-02-04',3);
insert into MoviesInTheaters values(4,'Veer-Zara',2018,'["Drama","Romance"]','[2,10,1,6,5,7]','R','PT99M','2018-11-28',5);
insert into MoviesInTheaters values(5,'Sarkar',2017,'["Action","Thriller","Crime"]','[9,10,5,6,9,7]','11','PT98M','2017-06-13',4);

    select * from MoviesInTheaters;
    
create table TopRatedIndian(id int primary key,title varchar(20),year int ,genres JSON ,ratings JSON,contentRating varchar(10),duration varchar(10),releaseDate varchar(10),imdbRating int);
insert into TopRatedIndian values(1,'Dear Zindagi',2017,'["Drama"]','[9,10,5,6,9,7]','11','PT98M','2017-06-13',4);
insert into TopRatedIndian values(2,'Madaari',2016,'["Drama","Thriller"]','[2,4,5,6,9,8]','R','PT56M','2016-06-09',5);
insert into TopRatedIndian values(3,'Peepli',2017,'["Action","Thriller","Crime"]','[9,10,5,6,9,7]','11','PT98M','2017-06-13',4);
insert into TopRatedIndian values(4,'Ghajini',2016,'["Action","Thriller"]','[5,10,8,6,9,7]','R','PT63M','2016-02-04',3);
insert into TopRatedIndian values(5,'Mankatha',2019,'["Action","Thriller","Crime"]','[7,8,5,6,9,7]','11','PT78M','2019-07-26',5);
 
     select * from TopRatedIndian;
     
 create table TopRatedMovies(id int primary key,title varchar(20),year int ,genres JSON ,ratings JSON,contentRating varchar(10),duration varchar(10),releaseDate varchar(10),imdbRating int);
 insert into TopRatedMovies values(1,'Black Panthar',2017,'["Action","Thriller","Crime"]','[9,10,5,6,9,7]','11','PT98M','2017-06-13',4);
 insert into TopRatedMovies values(2,'Grottmannen Dug',2016,'["Action","Thriller"]','[5,10,8,6,9,7]','R','PT63M','2016-02-04',3);
 insert into TopRatedMovies values(3,'Aiyaary',2018,'["Drama","Romance"]','[2,10,1,6,5,7]','R','PT99M','2018-11-28',5);
 insert into TopRatedMovies values(4,'Samson',2016,'["Action","Thriller"]','[5,10,8,6,9,7]','R','PT63M','2016-02-04',3);
 insert into TopRatedMovies values(5,'Loveless',2016,'["Action","Thriller"]','[6,8,8,6,9,7]','R','PT89M','2016-11-08',4);
 
       select * from TopRatedMovies;
       
  create table Favourite(id int primary key,title varchar(20),year int ,genres JSON ,ratings JSON,contentRating varchar(10),duration varchar(10),releaseDate varchar(10),imdbRating int);
  insert into Favourite values(1,'Ayan',2018,'["Drama","Romance"]','[2,10,1,6,5,7]','R','PT99M','2018-11-28',5);
  insert into Favourite values(2,'Gunda',2017,'["Action","Thriller","Crime"]','[9,10,5,6,9,7]','11','PT98M','2017-06-13',4);
  insert into Favourite values(3,'D-Day',2016,'["Action","Thriller"]','[6,8,8,6,9,7]','R','PT89M','2016-11-08',3);
  insert into Favourite values(4,'Nanban',2015,'["Drama","Action"]','[8,10,1,6,5,7]','R','PT67M','2015-05-28',5);
  insert into Favourite values(5,'Paa',2016,'["Drama","Thriller"]','[2,4,5,6,9,8]','R','PT56M','2016-06-09',3);
  
          select * from  Favourite;
    
   
   
 
  
 
 
